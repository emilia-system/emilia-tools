#!/bin/bash

# Minimal to Desktop. Not suposed to be run except in special ocasions.

pacman -Sy xorg-server lightdm-gtk-greeter --noconfirm --needed

systemctl enable lightdm.service
systemctl set-default graphical.target; 

emilia-garden reset $1
emilia-garden reset lightdm
dconf update

# Identical to configure desktop.sh

if [ "$1" = "mate" ]; then
    emilia-create-autostart \
                -t i \
                -c "emilia-garden reset mate" \
                -n "Appearance fix for mate" \
                -d "Mate is not configured by default and we couldn't fix it yet." \
                -a "/etc/skel/.config/autostart/"
elif [ "$1" = "gnome" ]; then
    mkdir -p /etc/skel/.config/autostart
    emilia-create-autostart \
        -t i \
        -c "/usr/share/emilia/gnome-setup.sh" \
        -n "gnome-setup" \
        -d "Initialize Gnome" \
        -a "/etc/skel/.config/autostart/"
    
    id -u emiliayay > /dev/null || useradd -r -u 2206 -m -s /usr/bin/nologin emiliayay
    id -u emiliayay > /dev/null && { 
        gpasswd -a emiliayay admin
        sudo -u emiliayay yay -Sy gnome-shell-extension-dash-to-panel --noconfirm --needed
        userdel -rf emiliayay
    } 
fi

touch /test.txt
