#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#include "emiso_tools.h"

int isLiveIncluded = 0;
int isTable = 0;
char pkgDir[64];

void help(int exit_error){
    puts("uso: $ emiso-package-data <op��o> <pasta onde estao os arquivos>");
    puts("");
    puts("Este programa � feito para ser usado junto da pasta dos pacotes");
    puts("gerados pelos scripts da emilia para gerar as isos. E apenas isso!");
    puts("Op��es:");
    puts("");
    puts("    list-all -> Lista todos os pacotes, ordenados por alfabeto e DE.");
    puts("    list-all-live -> Igual acima, mas com Live no meio");
    puts("   -> Para ambas acima: 1 -> possui no comum da distro e 2-> possui apenas na Live");
    puts("   -> Com excess�o da ultima (tt), que � em quantas distros aparecem.");
    puts("");
    puts("    table -> Gera uma tabela inteira no formato .csv para abertura em outros programas.");
    puts("    table-live -> Igual Acima mas inclui Live.");
    puts("   -> C � para Common e L para Live. e B para Base, claro.");
    puts("   -> recomenda-se usar redirecionamento (>) para jogar em um arquivo .csv, para n�o vir ao terminal");
    puts("");
    puts("    help -> Este menu que est� observando.");



    puts("S� um lembrete, ele joga tudo pra stdout. quer em arquivo? pipe.");
    exit(exit_error);
}

void option_process(int argc, char *argv[]){
    if (argc < 2){ //caso seja help, agr vai rodar ao menos.
        puts("ERRO: Nenhum Argumento. Forne�a o tipo de opera��o e a pasta onde est�o os arquivos ou pe�a ajuda");
        help(1);
    }

    //valida a opera��o escolhida. switch em C n�o roda com string.
    if(strcmp(argv[1],"list-all") == 0){
        //liste todos os pacotes envolvidos.
        isTable = 0;
        isLiveIncluded = 0;
    }
    else if(strcmp(argv[1],"list-all-live") == 0){
        //igual acima, mas inclui live.
        isTable = 0;
        isLiveIncluded = 1;
    }
    else if(strcmp(argv[1],"table") == 0){
        //tabela comparando onde o pacote aparece nas listas
        isTable = 1;
        isLiveIncluded = 0;
    }
    else if(strcmp(argv[1],"table-live") == 0){
        //igual acima, mas a coluna "live" pra cada um aparece
        isTable = 1;
        isLiveIncluded = 1;
    }
    else if(strcmp(argv[1],"help") == 0){
        //fun��o que printa ajuda.
        help(0);
    }
    else {
        puts("ERRO: Opera��o inv�lida. Veja novamente a lista de op��es na ajuda:");
        help(1);
    }
//-------
    if (argc < 3){
        puts("ERRO: Poucos Argumentos. Forne�a o tipo de opera��o e a pasta onde est�o os arquivos");
        help(1);
    }
    else if (argc > 4) {
        puts("ERRO: Muitos Argumentos. Forne�a apenas o tipo de opera��o e a pasta onde est�o os arquivos");
        help(1);
    }

    strcpy(pkgDir,argv[2]); //diret�rio no lugar

}
//base - common e live tem todos dele.
//common - live
//script da emilia usa package_list como padr�o.

//verdadeiro se existir!
FILE* get_file(const char* file, char prefix){
    char FileName[256];
    char Sprefix[2] = {prefix,'\0'};
    char barra[2] = "/";

    strcpy(FileName,pkgDir);
    if(FileName[strlen(FileName)-1] != '/'){
        //adiciona a barra pq usu�rio esqueceu...
        strcat(FileName,barra);
    }
    strcat(FileName,Sprefix);
    strcat(FileName,file);
    strcat(FileName,".txt"); //fica PDesktop.txt
    FILE* data = fopen(FileName,"r");
    //printf("%s\n",FileName);
    return data;
}

Packages* inser_in_list(Packages* lista, const char *file, int pos_desktop, char prefix){
    FILE* temp = get_file(file,prefix);
    if(temp != NULL){
        char name[64];
        char versao[64];
        int desk;
        while(!feof(temp)){
            //a cada linha � "nome vers�o" e "\n"
            //cada um deles � um elemento diferente de lista.
            fscanf(temp,"%s %s\n",name,versao);
            //insere na lista
            lista = insert_package_list(lista,name,versao,pos_desktop,prefix);
        }
        //show_package_list(lista);
    }
    return lista;
}

int main(int argc, char *argv[]){

    setlocale(LC_ALL,"");
    //tratamento de argv:
    //pode apenas 3. nome do programa, opera��o e pasta
    option_process(argc,argv);

    Packages* lista = create_package_list();

    //pro pacote base
    lista = inser_in_list(lista,DESKTOP_ENVIROMENTS[0],0,'\0');
    char prefix = 'C';
    int i;
    for (i=1;i<NUM_DESKTOP_ENV;i++){
        //pra cada um dos desktop enviroment.
        lista = inser_in_list(lista,DESKTOP_ENVIROMENTS[i],i,prefix);
    }

    if(isLiveIncluded == 1){
        prefix = 'L';
        for (i=1;i<NUM_DESKTOP_ENV;i++){
            lista = inser_in_list(lista,DESKTOP_ENVIROMENTS[i],i,prefix);
        }
    }

    //------------------------------------------------------------------------
    //agr � gerar a saida e gg

    if(!isTable) show_package_list(lista);
    else show_package_table(lista);

    return 0;
}
