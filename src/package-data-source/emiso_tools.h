#ifndef EMISO_TOOLS_H
#define EMISO_TOOLS_H

//Lista de Desktop Enviroments. Util pra Emilia
#define NUM_DESKTOP_ENV 9
#define DESKTOP_ENVIROMENTS (const char*[NUM_DESKTOP_ENV]) {"base","minimal","gnome","plasma","xfce","cinnamon","mate","lxqt","budgie"}


struct package_data {
    char name[64];
    char version[64];
    int in_flavors[NUM_DESKTOP_ENV]; //� a lista dos Desktop, incluindo o base no 0; caso tenha 1 ele possui, se tem 2 tem no live

    struct package_data* prev;
    struct package_data* next;
};

typedef struct package_data Packages;

int desktop_to_pos(char *desktop);

Packages* create_package_list();
Packages* insert_package_list(Packages* lista, char* nome, char* version, int desktop_pos, char prefix);
Packages* search_package_list(Packages* lista, char* nome);
void show_package_list(Packages* lista);
void show_package_table(Packages* lista);
#endif // EMISO_TOOLS_H
