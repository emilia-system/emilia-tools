#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "emiso_tools.h"

//fun��es

int strlen_extra(char* string){
    int quanti = 0;
    while(string[quanti] !='\0'){
        quanti++;
    }
    return quanti;
}


Packages* create_package_list(){
    return NULL;
}

Packages* insert_package_list(Packages* lista, char *nome,char *version,int desktop_pos, char prefix){
    //testar para ver se acha ele mesmo
        //se o que ele t� o strcmp der negativo e o pr�ximo positivo, ele n�o existe e entra ali.
        //se for 0, aquela � a posi��o dele!

    //depois de inserido/detectado, ver no desktop_pos e marque de acordo com um =1. se j� existia n faz diferen�a mais kkk

    Packages* aux = search_package_list(lista, nome);
    if(aux == NULL) // ent�o � novo pacote!
    {
        Packages* novo = malloc(sizeof(Packages));
        strcpy(novo -> name, nome);
        strcpy(novo -> version, version);

        int i;
        for(i=0;i<9;i++) novo-> in_flavors[i] = 0; //todos iniciam com 0;
        if(prefix == 'C') novo -> in_flavors[desktop_pos] += 1;
        else novo -> in_flavors[desktop_pos] += 2;

        //e agr insere na pos. alfab�tica.
        //aux � nulo
        //se for o primeiro elemento, insere e retorna ele. sen�o retorna lista original!
        if(lista == NULL){
            //caso esteja vazia. esse cara � o primeiro elemento. Fim de papo.
            return novo;

        }
        else{

            //testa caso tenha de entrar antes do primeiro
            if(strcmp(novo -> name, lista -> name) < 0){
                lista -> prev = novo;
                novo -> next = lista;
                novo -> prev = NULL;
                return novo;
            }
            aux = lista;
            //ap�s o elemento inicial

            while(aux -> next != NULL){
                if(strcmp(novo -> name, aux -> next -> name) < 0){
                    break; //aux para no anterior ao que precisamos add.
                }
                aux = aux -> next;
            }
            //� para inserir entre este e o pr�ximo que pode ser nulo.
            //printf("%s",novo->name);
            novo -> prev = aux;
            novo -> next = aux -> next;
            //puts("ok");
            if(aux -> next != NULL) aux -> next -> prev = novo;
            aux -> next = novo;

            return lista; //n mudei ela mesmo, mas preciso retornar.
        }

    }
    else
    {
        //ent�o podemos pegar a pos. dela no aux e add no desktop_pos.
        if(prefix == 'C')aux -> in_flavors[desktop_pos] += 1;
        else aux -> in_flavors[desktop_pos] += 2;

        return lista; //TODO - caso for primeiro elemento.
    }

}

Packages* search_package_list(Packages* lista, char* nome)
{
    Packages* aux = lista;
    if(aux ==  NULL) return NULL; //se primeiro for NULL! Vazia!

    while(strcmp(nome, aux -> name) != 0)
    {
        aux = aux -> next;
        if(aux ==  NULL) return NULL;
    }
    return aux; //ele est� no que � igual, ent�o....
}

void show_package_list(Packages* lista)
{
    Packages* aux = lista;
    int i,total=0;
    while(aux != NULL)
    {
        total = 0;

        printf("%s - %s", aux->name, aux->version);
        int espacos = 60 - strlen_extra(aux->name) - strlen_extra(aux->version);
        for(i=0;i<espacos;i++) printf("-");

        if(aux->in_flavors[0] == 2) aux->in_flavors[0] = 1; //bug estranho, mas fazer o q. isso s� troca o 2 pelo 1 caso seja 2. apenas visual
        for(i=0;i<NUM_DESKTOP_ENV;i++){
            printf(" %c:%d", DESKTOP_ENVIROMENTS[i][0], aux->in_flavors[i]);

            if(i != 0 && (aux->in_flavors[i] == 1 || aux->in_flavors[i] == 3)){//base n�o conta, vale pro caso da live tb
                total++;
            }
        }

        printf(" - tt:%d\n",total);
        aux = aux -> next;
    }
}

void show_package_table(Packages* lista)
{
    int i,j, total = 0;

    //termina o cabe��rio
    printf("package;version");
    for(i=0;i<NUM_DESKTOP_ENV;i++){
        printf(";%s",DESKTOP_ENVIROMENTS[i]);
    }
    printf(";total\n");
    Packages* aux = lista;
    while(aux != NULL)
    {
        total = 0;
        printf("%s;%s",aux->name, aux->version);
        if(aux ->in_flavors[0] != 0){
            printf(";B");
            total = NUM_DESKTOP_ENV-1;//pq base n conta;
            for(i=1;i<NUM_DESKTOP_ENV;i++) printf(";");
        }
        else {
            printf(";");
            for(i=1;i<NUM_DESKTOP_ENV;i++){
                switch(aux->in_flavors[i])
                {
                    case 1: case 3: //pq todos os 3 s�o common
                        printf(";C");
                        total++;
                        break;
                    case 2:
                        printf(";L");
                        break;
                    case 0: default:
                        printf(";");
                        break;
                }
            }
        }
        printf(";%d\n",total);
        aux = aux -> next;
    }
}
