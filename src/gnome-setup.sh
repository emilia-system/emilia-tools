#!/bin/bash

# Setup Extensions. & shell-user-theme
# Just to be sure
gsettings set org.gnome.shell disable-user-extensions false 

gnome-shell-extension-tool -e user-theme@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e alternate-tab@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e launch-new-instance@gnome-shell-extensions.gcampax.github.com
gnome-shell-extension-tool -e drive-menu@gnome-shell-extensions.gcampax.github.com

gnome-shell-extension-tool -e dash-to-panel@jderose9.github.com
