#/usr/bin/env bash

## Inspired by https://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html
## This file is an implementation from the site. Helped a lot!
_update_mirrors_completions()
{
    if [ "${#COMP_WORDS[@]}" != "2" ]; then
        return
    fi
    
    local IFS=$'\n'
    local __all_language_codes=(
        "AU - Australia"
        "AT - Austria"
        "BD - Bangladesh"
        "BY - Belarus"
        "BE - Belgium"
        "BA - Bosnia and Herzegovina"
        "BR - Brazil (default if none)"
        "CA - Canada"
        "CL - Chile"
        "CN - China"
        "CO - Colombia"
        "HR - Croatia"
        "CZ - Czechia"
        "DK - Denmark"
        "EC - Ecuador"
        "FI - Finland"
        "FR - France"
        "GE - Georgia"
        "DE - Germany"
        "GR - Greece"
        "HK - Hong Kong"
        "HU - Hungary"
        "IS - Iceland"
        "IN - India"
        "ID - Indonesia"
        "IR - Iran"
        "IE - Ireland"
        "JP - Japan"
        "KZ - Kazakhstan"
        "KE - Kenya"
        "LV - Latvia"
        "LT - Lithuania"
        "LU - Luxembourg"
        "MX - Mexico"
        "NL - Netherlands"
        "NC - New Caledonia"
        "NZ - New Zealand"
        "MK - North Macedonia"
        "NO - Norway"
        "PY - Paraguay"
        "PH - Philippines"
        "PL - Poland"
        "PT - Portugal"
        "RO - Romania"
        "RU - Russia"
        "RS - Serbia"
        "SG - Singapore"
        "SK - Slovakia"
        "SI - Slovenia"
        "ZA - South Africa"
        "KR - South Korea"
        "ES - Spain"
        "SE - Sweeden"
        "CH - Switzerland"
        "TW - Taiwan"
        "TH - Thailand"
        "TR - Turkey"
        "UA - Ukraine"
        "GB - United Kingdom"
        "US - United States (default. ALWAYS)"
        "VN - Vietnam"
    )
    local show=($(compgen -W "$(printf '%s\n' "${__all_language_codes[@]}")" -- "${COMP_WORDS[1]}"))
    if [ "${#show[@]}" == "1" ]; then
        local code=$(echo ${show[0]%% *})
        COMPREPLY=("$code")
    else
        for i in "${!show[@]}"; do
            show[$i]="$(printf '%*s' "-$COLUMNS"  "${show[$i]}")"
        done
        COMPREPLY=("${show[@]}")
    fi
    
}

complete -F _update_mirrors_completions update-mirrors
