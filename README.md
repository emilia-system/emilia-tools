# Emilia Tools

[![pipeline status](https://gitlab.com/emilia-system/emilia-tools/badges/master/pipeline.svg)](https://gitlab.com/emilia-system/emilia-tools/commits/master)
[![License](https://img.shields.io/badge/license-MPL2-orange)](https://choosealicense.com/licenses/mpl-2.0/)

For every user, there are some tools that could make life easy from time to time.
On this repository, some tools are created for aiding Emilia users.

## Installation
They are all independent scripts, all of them could work without previous installation. All of them have an -h for help.

### update-mirrors

In pacman, there are default repositorys from install. However, they may be not the fastest for your location and network.
To solve that, Arch created a tool on their website for creating a config file and put the fastest connections first.
This tool only automates this substitution.

### make-adapta

In the Emilia Daisy and extra-themes, they use this script for their generation. This script grabs the source from both Adapta themes and changes their color to then rename to the desired name.

### More tools planned in the future.

## Contributing

We are open to suggestions and are always listening for bugs when possible. For some big change, please start by openning an issue so that it can be discussed.

## Known problems

## Licensing

Emilia uses MPL2 for every program created by the team and the original license if based on another program. In this case:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

# Emilia Tools - pt_BR

Para todo usuário, há certas ferramentas que facilitariam a vida de vez em quando.
Neste repositório, algumas ferramentas são criadas para auxílio dos usuários da Emilia.

## Instalação
São todos scripts independentes, todos funcionariam sem instalação prévia. Todos possuem um -h para ajuda.

### update-mirrors

No pacman, há repositórios padrões de instalação, porém, eles podem não ser os mais rápidos para sua localização e rede.
Para resolver isso, o Arch criou uma ferramenta no site deles para criar um arquivo de configuração e colocar as conexões mais rápidas primeiro.
Esta ferramenta só automatiza a substituição dela.

### make-adapta

No pacote Emilia Daisy e extra-themes, eles utilizam este script para serem gerados. Este script pega a fonte de ambos temas Adaptas e os troca de cor para então renomear para o nome customizado.

### Mais ferramentas planejadas no futuro.

## Contribuindo

Somos abertos a sugestões e estamos sempre escutando por bugs quando possível. Para alguma mudança grande, por favor comece abrindo um issue na página para que possa ser discutido.

## Problemas conhecidos

## Licença

Emilia usa MPL2 para todos os programas criados pelo time e a licença original caso baseado em outro programa. No caso deste:

[MPL2](https://choosealicense.com/licenses/mpl-2.0/)

